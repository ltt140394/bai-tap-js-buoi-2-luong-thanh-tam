/*
Bài tập 1:

Output: Tạo 2 biến lấy dữ liệu tiền lương trong 1 ngày và số ngày làm người dùng cần tính.

Step:
.Step 1: Chuyển đổi dạng dữ liệu string nhập vào sang dạng number.
.Step 2: Tạo 1 biến lưu giá trị tổng lương. Tính lương theo công thức: Lương = lương 1 ngày * số ngày làm.

Output: Xuất kết quả vừa tính ra text.
*/
function salary() {
  var salaryOneDay = document.getElementById("salary_one_day").value * 1;
  var dayOfWork = document.getElementById("day_of_work").value * 1;
  var result = salaryOneDay * dayOfWork;
  var resultFormatter = new Intl.NumberFormat('vn-VN').format(result);
  document.getElementById("total_salary").innerHTML = `
    <label>Tổng lương :</label>
    <input type="text" value="${resultFormatter} VND" readonly class="form-control mb-4" placeholder="" aria-describedby="helpId">
    `;
}

/*
Bài tập 2:

Output: Tạo 5 biến lấy dữ liệu 5 số người dùng cần tính.

Step:
.Step 1: Chuyển đổi dạng dữ liệu string nhập vào sang dạng number.
.Step 2: Tạo 1 biến lưu giá trị trung bình. Giá trị trung bình được tính theo công thức: GTTB = tổng 5 số thực chia 5.

Output: Xuất kết quả vừa tính ra text.
*/
function average() {
  var number1 = document.getElementById("number_1").value * 1;
  var number2 = document.getElementById("number_2").value * 1;
  var number3 = document.getElementById("number_3").value * 1;
  var number4 = document.getElementById("number_4").value * 1;
  var number5 = document.getElementById("number_5").value * 1;
  var result = (number1 + number2 + number3 + number4 + number5) / 5;
  document.getElementById("average").innerHTML = `
    <label>Giá trị trung bình :</label>
    <input type="text" value="${resultFormatter}" readonly class="form-control mb-4" placeholder="" aria-describedby="helpId">
    `;
}

/*
Bài tập 3:

Output: Tạo 1 biến lấy dữ liệu số USD người dùng cần đổi.

Step:
.Step 1: Chuyển đổi dạng dữ liệu string nhập vào sang dạng number.
.Step 2: Tạo 1 biến lưu giá trị tiền Việt sau khi đổi, đc tính theo công thức: Tiền Việt = số USD * 23500.

Output: Xuất kết quả vừa tính ra text.
*/
function changeMoney() {
  var usd = document.getElementById("usd").value * 1;
  var result = usd * 23500;
  var resultFormatter = new Intl.NumberFormat('vn-VN').format(result);
  document.getElementById("change_money").innerHTML = `
    <label>Tiền Việt :</label>
    <input type="text" value="${resultFormatter} VND" readonly class="form-control mb-4" placeholder="" aria-describedby="helpId">
    `;
}

/*
Bài tập 4:
Output : Tạo 2 biến lấy dữ liệu chiều dài và chiều rộng người dùng cần tính.

Step:
.Step 1: Chuyển đổi dạng dữ liệu string nhập vào sang dạng number.
.Step 2: Tạo 1 biến lưu giá trị diện tích HCN, đc tính theo công thức: Diện tích HCN = dài * rộng.
.Step 3: Tạo 1 biến lưu giá trị chu vi HCN, đc tính theo công thức: Chu vi HCN = (dài + rộng) * 2.

Ouput: Xuất kết quả vừa tính ra text.
*/
function rectangle() {
  var widthRectangle = document.getElementById("width_rectangle").value * 1;
  var heightRectangle = document.getElementById("height_rectangle").value * 1;
  var area = widthRectangle * heightRectangle;
  var perimeter = (widthRectangle + heightRectangle) * 2;
  document.getElementById("rectangle").innerHTML = `
    <label>Diện tích HCN :</label>
    <input type="text" value="${area} cm2" readonly class="form-control mb-4" placeholder="" aria-describedby="helpId">
    <label>Chu vi HCN :</label>
    <input type="text" value="${perimeter} cm" readonly class="form-control mb-4" placeholder="" aria-describedby="helpId">
    `;
}

/*
Bài tập 5:
Ouput: Tạo 1 biến lấy dữ liệu số nguyên dương người dùng cần tính.

Step:
.Step 1: Chuyển đổi dạng dữ liệu string nhập vào sang dạng number.
.Step 2: Tạo 1 biến lưu giá trị số hàng chục.
Lấy output chia cho 10, làm tròn số ta đc số hàng chục.
.Step 3: Tạo 1 biến lưu giá trị số hàng đơn vị.
Lấy output chia dư cho 10, lấy số dư ta đc hàng đơn vị.
*/
function sumElementNumber() {
  var integer = document.getElementById("integer").value * 1;
  var tenClass = Math.floor(integer / 10);
  var unit = integer % 10;
  var result = tenClass + unit;
  document.getElementById("sum").innerHTML = `
    <label>Tổng các ký số :</label>
    <input type="text" value="${integer} => ${tenClass} + ${unit} = ${result}" readonly class="form-control mb-4" placeholder="" aria-describedby="helpId">
    `;
}
